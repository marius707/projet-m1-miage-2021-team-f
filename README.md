# Présentation

Ce projet conclut le module d'enseignement programmation web dans l'optique d'en valider les acquis. Il s'agit d'une API node js de gestion de listes de courses avec ajout/modification/suppression de produit, consultation de liste complète de produit avec leurs caractéristiques principales. L'API est développé avec les framework [express](https://expressjs.com/fr/), [mongoose](https://mongoosejs.com/) et le middleware [passport.js](http://www.passportjs.org/) ainsi Mongodb est la base où les données sont persistées. HTTPS est configuré comme en témoigne les fichiers server.cert et server.key.

L'API comprend deux modèles de données, les listes de produits et les utilisateurs.

Les listes de produits se caractérisent par :

    - Un titre

    - Des produits :

        - un nom

        - une description

        - une marque

        - une quantité

        - un prix

Les utilisateurs :

    - email 

    - password

## Installation

Commençons par installer les prérequis pour le bon fonctionnement du projet.

Node.js et npm : https://www.npmjs.com/get-npm 

MongoDB : https://www.mongodb.com/fr

Rendez-vous dans le dossier où vous souhaitez avoir le projet et récupérer le clone Gitlab du projet 
```bash
cd "pathwhereiwantmyproject"
git clone git@gitlab.com:marius707/projet-m1-miage-2021-team-f.git
```

Mongoose est nécessaire pour l'accès à la base donnée MongoDB, il est possible de l'installer avec la commande suivante :
```bash
npm install mongoose
```
Passport est à installer pour la fonctionnalité d'authentification :
```bash
npm install passport
```

## Utilisation

Pour lancer le projet faites la commande suivante :
```bash
npm start
```
Ensuite je vous invite à poursuivre sur [postman](https://www.postman.com/)

Ci-dessous les routes utilisables : 

```bash
#POST pour se connecter 
#Route accessible sans token d'authentification
https://localhost:3000/login/
``` 
```bash
#POST pour créer un nouvel utilisateur
#Route accessible sans token d'authentification
https://localhost:3000/singin/
``` 
```bash
#GET pour avoir les informations de l'utilisateur connecté
https://localhost:3000/current/
``` 
```bash
#GET pour avoir toutes les listes
#POST pour ajouter une liste
#DELETE pour supprimer toutes les listes
https://localhost:3000/products/
``` 
```bash
#GET pour avoir le produit,
#PUT pour modifier le produit
#DELETE pour le supprimer
https://localhost:3000/products/:id
``` 
