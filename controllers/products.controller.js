const Product = require("../models/product.model")

let controller = {
    getAll: async (req, res, next) => {
        let products = await Product.find({});
        res.json(products);
    },
    addOne: async (req, res, next) => {
        let product = await Product.create(req.body);
        res.json(product);
    },
    deleteAll: async (req, res, next) => {
        let resp = await Product.remove({});
        res.json(resp);
    },
    getOne: async (req, res, next) => {
        let product = await Product.findById(req.params.id);
        res.json(product);
    },
    updateOne: async (req, res, next) => {
        let product = await Product.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.json(product);
    },
    deleteOne: async (req, res, next) => {
        let resp = await Product.findByIdAndRemove(req.params.id);
        res.json(resp);
    }
};

module.exports = controller;