const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productSchema = new Schema(
    {
        title: {type: String},
        products: [{
            name: {type: String},
            description: {type: String},
            brand: {type: String},
            quantity: {type: Number},
            price: {type: Number, default: -1.0}
        }]
    },
    {
        collection: "products", timestamps: true
    }
);

module.exports = mongoose.model("Product", productSchema);