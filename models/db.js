const mongoose = require('mongoose');

const url = "mongodb://localhost:27017/products-db"

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

mongoose.connect(url, options);

mongoose.connection.on("connecting", () => {
    console.log("Connecting");
});
mongoose.connection.on("error", () => {
    console.log("Connection error");
});
mongoose.connection.on("connected", () => {
    console.log("Connection established");
});
mongoose.connection.on("disconnected", () => {
    console.log("Disconnected");
});
mongoose.connection.on("reconnected", () => {
    console.log("Reconnected");
});

module.exports = mongoose.connection;