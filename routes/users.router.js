const express = require('express');
const controller = require("../controllers/users.controller");
const router = express.Router();
const auth = require('./auth');

//POST new user route (optional, everyone has access)
router.post('/signin', auth.optional, controller.signin);
//POST login route (optional, everyone has access)
router.post('/login', auth.optional, controller.login);
//GET current route (required, only authenticated users have access)
router.get('/current', auth.required, controller.current);

module.exports = router;