const express = require("express");
const controller = require("../controllers/products.controller");
var router = express.Router();
const auth = require('./auth');

// Add Passport Module
router.all("/",(req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Content-type", "text/plain");
        next();
    })

router.get("/", auth.required, controller.getAll)
router.post("/", auth.required, controller.addOne)
router.delete("/", auth.required, controller.deleteAll);

router.get("/:id", auth.required, controller.getOne)
router.put("/:id", auth.required, controller.updateOne)
router.delete("/:id", auth.required, controller.deleteOne);

module.exports = router;